﻿using System;
using System.Threading.Tasks;
using AskarTecTest.DAL.Repositories;
using AskarTecTest.DAL.Repositories.Contracts;

namespace AskarTecTest.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IClientRepository Clients { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Clients = new ClientRepository(context);
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
