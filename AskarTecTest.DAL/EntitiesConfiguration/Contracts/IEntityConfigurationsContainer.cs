﻿using AskarTecTest.DAL.Entities;

namespace AskarTecTest.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Client> ClientConfiguration { get; }
    }
}
