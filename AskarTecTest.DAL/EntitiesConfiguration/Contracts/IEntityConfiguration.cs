﻿using System;
using AskarTecTest.DAL.Entities.Contracts;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AskarTecTest.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
