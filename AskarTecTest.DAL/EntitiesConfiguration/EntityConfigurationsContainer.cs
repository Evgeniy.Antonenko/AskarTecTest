﻿using AskarTecTest.DAL.Entities;
using AskarTecTest.DAL.EntitiesConfiguration.Contracts;

namespace AskarTecTest.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Client> ClientConfiguration { get; }


        public EntityConfigurationsContainer()
        {
            ClientConfiguration = new ClientConfiguration();
        }
    }
}
