﻿using AskarTecTest.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AskarTecTest.DAL.EntitiesConfiguration
{
    public class ClientConfiguration : BaseEntityConfiguration<Client>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Client> builder)
        {
            builder
                .Property(c => c.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(c => c.BirthDate)
                .IsRequired();

            builder
                .Property(c => c.PhoneNumber)
                .HasMaxLength(13)
                .IsRequired();

            builder
                .Property(c => c.Address)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            builder
                .Property(c => c.SocialNumber)
                .HasMaxLength(14)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Client> builder)
        {
        }
    }
}
