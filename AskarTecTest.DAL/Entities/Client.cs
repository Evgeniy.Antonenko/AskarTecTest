﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using AskarTecTest.DAL.Entities.Contracts;

namespace AskarTecTest.DAL.Entities
{
    public class Client : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [Column(TypeName = "Date")]
        public DateTime BirthDate { get; set; }

        public string PhoneNumber { get; set; }

        public string Address { get; set; }

        public string SocialNumber { get; set; }
    }
}
