﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AskarTecTest.DAL.Entities.Contracts;
using Microsoft.AspNetCore.Identity;

namespace AskarTecTest.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }
    }
}
