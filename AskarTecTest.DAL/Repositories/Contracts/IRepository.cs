﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AskarTecTest.DAL.Entities.Contracts;

namespace AskarTecTest.DAL.Repositories.Contracts
{
    public interface IRepository<T> where T : class, IEntity
    {
        T Create(T entity);

        T GetById(int id);

        IEnumerable<T> GetAll();

        T Update(T entity);

        void Remove(T entity);

        Task<T> GetByIdAsync(int id);

        Task CreateAsync(T entity);
    }
}
