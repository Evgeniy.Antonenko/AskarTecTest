﻿using System.Collections.Generic;
using AskarTecTest.DAL.Entities;

namespace AskarTecTest.DAL.Repositories.Contracts
{
    public interface IClientRepository : IRepository<Client>
    {
        IEnumerable<Client> GetUsersWithoutById(int id);
        Client GetByPhoneNumber(string phoneNumber);
        Client GetBySocialNumber(string socialNumber);
        IEnumerable<Client> GetByClientName(string name);
    }
}
