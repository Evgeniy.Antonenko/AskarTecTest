﻿using System.Collections.Generic;
using System.Linq;
using AskarTecTest.DAL.Entities;
using AskarTecTest.DAL.Repositories.Contracts;

namespace AskarTecTest.DAL.Repositories
{
    public class ClientRepository : Repository<Client>, IClientRepository
    {
        public ClientRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Clients;
        }

        public IEnumerable<Client> GetByClientName(string name)
        {
            return entities.Where(c => c.Name == name).ToList();
        }

        public Client GetBySocialNumber(string socialNumber)
        {
            return entities.FirstOrDefault(c => c.SocialNumber == socialNumber);
        }

        public Client GetByPhoneNumber(string phoneNumber)
        {
            return entities.FirstOrDefault(c => c.PhoneNumber == phoneNumber);
        }

        public IEnumerable<Client> GetUsersWithoutById(int id)
        {
            return entities.Where(rp => rp.Id != id).ToList();
        }
    }
}
