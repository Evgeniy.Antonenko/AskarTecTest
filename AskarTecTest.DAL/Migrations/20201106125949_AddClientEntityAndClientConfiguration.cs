﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AskarTecTest.DAL.Migrations
{
    public partial class AddClientEntityAndClientConfiguration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    BirthDate = table.Column<DateTime>(type: "Date", nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 13, nullable: false),
                    Address = table.Column<string>(maxLength: 2147483647, nullable: false),
                    SocialNumber = table.Column<string>(maxLength: 14, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Clients");
        }
    }
}
