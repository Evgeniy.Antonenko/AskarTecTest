﻿
namespace AskarTecTest.DAL
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
