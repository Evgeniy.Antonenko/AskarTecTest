﻿using System.Collections.Generic;
using AskarTecTest.Validation.Contracts;
using AskarTecTest.Validation.Models;

namespace AskarTecTest.Validation
{
    public abstract class Validator<T> where T : IValidated
    {
        protected T validatedModel;
        protected List<ValidationItem> validationItems;

        protected Validator()
        {
            validationItems = new List<ValidationItem>();
        }

        protected abstract void SetValidationItems();

        public ValidationResult Validate(T validatedModel)
        {
            this.validatedModel = validatedModel;
            SetValidationItems();
            ValidationResult result = new ValidationResult();
            foreach (var item in validationItems)
            {
                if (item.Predicate)
                    result.AddError(item.ErrorMessage);
            }

            return result;
        }
    }
}
