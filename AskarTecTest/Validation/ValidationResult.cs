﻿using System.Collections.Generic;
using System.Linq;

namespace AskarTecTest.Validation
{
    public class ValidationResult
    {
        public bool IsSuccess => !ErrorMessages.Any();

        public List<string> ErrorMessages { get; }

        public ValidationResult()
        {
            ErrorMessages = new List<string>();
        }

        public void AddError(string error)
        {
            ErrorMessages.Add(error);
        }
    }
}
