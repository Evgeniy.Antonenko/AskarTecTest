﻿
namespace AskarTecTest.Validation.Models
{
    public class ValidationItem
    {
        public string ErrorMessage { get; set; }

        public bool Predicate { get; set; }
    }
}
