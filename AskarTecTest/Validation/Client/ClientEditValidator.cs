﻿using System.Linq;
using AskarTecTest.DAL;
using AskarTecTest.Models.ClientModels;
using AskarTecTest.Validation.Models;

namespace AskarTecTest.Validation.Client
{
    public class ClientEditValidator : Validator<ClientEditModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public ClientEditValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            string dateBirth = $"{validatedModel.BirthDate.Day:D2}{validatedModel.BirthDate.Month:D2}{validatedModel.BirthDate.Year}";
            validationItems.AddRange(new[]
            {
                new ValidationItem()
                {
                    Predicate = validatedModel.SocialNumber.Substring(1, 8) != dateBirth,
                    ErrorMessage = $"Инн {validatedModel.SocialNumber} не соответствует выбранной дате рождения {validatedModel.BirthDate.ToShortDateString()}"
                },
                ValidateClientByNameAndBirthDateInDb(),
                ValidatePinInDb(),
                ValidateMobilePhoneInDb()
            });
        }

        private ValidationItem ValidateClientByNameAndBirthDateInDb()
        {
            var isBool = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var otherClients = unitOfWork.Clients.GetUsersWithoutById(validatedModel.Id);
                var otherClientsWithByName = otherClients.Where(c => c.Name == validatedModel.Name);

                foreach (var client in otherClientsWithByName)
                {
                    if (client.BirthDate == validatedModel.BirthDate) isBool = false;
                }

                return new ValidationItem()
                {
                    Predicate = isBool == false,
                    ErrorMessage = $"Клиент {validatedModel.Name} уже существует"
                };
            }
        }

        private ValidationItem ValidatePinInDb()
        {
            var isBool = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var otherClients = unitOfWork.Clients.GetUsersWithoutById(validatedModel.Id);

                foreach (var client in otherClients)
                {
                    if (client.SocialNumber == validatedModel.SocialNumber) isBool = false;
                }

                return new ValidationItem()
                {
                    Predicate = isBool == false,
                    ErrorMessage = $"Клиент с таким ИНН {validatedModel.SocialNumber} уже существует"
                };
            }
        }

        private ValidationItem ValidateMobilePhoneInDb()
        {
            var isBool = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var otherClients = unitOfWork.Clients.GetUsersWithoutById(validatedModel.Id);

                foreach (var client in otherClients)
                {
                    if (client.PhoneNumber == validatedModel.PhoneNumber) isBool = false;
                }

                return new ValidationItem()
                {
                    Predicate = isBool == false,
                    ErrorMessage = $"Клиент с таким № телефона {validatedModel.PhoneNumber} уже существует"
                };
            }
        }
    }
}
