﻿using System.Linq;
using AskarTecTest.DAL;
using AskarTecTest.Models.ClientModels;
using AskarTecTest.Validation.Models;

namespace AskarTecTest.Validation.Client
{
    public class ClientCreateValidator : Validator<ClientCreateModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public ClientCreateValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            string dateBirth = $"{validatedModel.BirthDate.Day:D2}{validatedModel.BirthDate.Month:D2}{validatedModel.BirthDate.Year}";
            validationItems.AddRange(new[]
            {
                new ValidationItem()
                {
                    Predicate = validatedModel.SocialNumber.Substring(1, 8) != dateBirth,
                    ErrorMessage = $"ИНН клиента {validatedModel.SocialNumber} не соответствует веденной дате рождения клиента {validatedModel.BirthDate.ToShortDateString()}"
                },
                ValidateClientByNameAndBirthDateInDb(),
                ValidateSocialNumberInDb(),
                ValidatePhoneNumberInDb()
            });
        }

        private ValidationItem ValidateClientByNameAndBirthDateInDb()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var clientByName = unitOfWork.Clients.GetByClientName(validatedModel.Name);
                var clientByBirthDate = clientByName.FirstOrDefault(c => c.BirthDate == validatedModel.BirthDate);

                return new ValidationItem()
                {
                    Predicate = clientByBirthDate != null,
                    ErrorMessage = $"Клиент {validatedModel.Name} уже существует"
                };
            }
        }

        private ValidationItem ValidateSocialNumberInDb()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var client = unitOfWork.Clients.GetBySocialNumber(validatedModel.SocialNumber);

                return new ValidationItem()
                {
                    Predicate = client != null,
                    ErrorMessage = $"Клиент с таким ИНН {validatedModel.SocialNumber} уже существует"
                };
            }
        }

        private ValidationItem ValidatePhoneNumberInDb()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var client = unitOfWork.Clients.GetByPhoneNumber(validatedModel.PhoneNumber);

                return new ValidationItem()
                {
                    Predicate = client != null,
                    ErrorMessage = $"Клиент с таким № телефона {validatedModel.PhoneNumber} уже существует"
                };
            }
        }
    }
}
