﻿using System;
using System.ComponentModel.DataAnnotations;
using AskarTecTest.Validation.Contracts;

namespace AskarTecTest.Models.ClientModels
{
    public class ClientEditModel : IValidated
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле \"Фамилия, Имя, Отчество\" должно быть заполненно")]
        [Display(Name = "Фамилия, Имя, Отчество")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        [RegularExpression(@"^[ а-яА-Я]+$", ErrorMessage = "Используйте только буквы!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Поле \"Дата рождения\" должно быть заполненно")]
        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Поле \"Номер телефона\" должно быть заполненно")]
        [Display(Name = "Номер телефона")]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Поле \"Адрес клиента\" должно быть заполненно")]
        [Display(Name = "Адрес клиента")]
        [RegularExpression(@"^[ \()\ \.\ \-\ а-яА-Я]+$", ErrorMessage = "Используйте только буквы \"кириллицы\"!")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Поле \"ИНН клиента\" должно быть заполненно")]
        [Display(Name = "ИНН клиента")]
        [MaxLength(14, ErrorMessage = "Максимальная длина 14 символов!")]
        [RegularExpression(@"^[12][\d+]{13}$", ErrorMessage = "ИНН должен состоять из 14 цифр и начинаться на 0, 1 или 2")]
        public string SocialNumber { get; set; }
    }
}
