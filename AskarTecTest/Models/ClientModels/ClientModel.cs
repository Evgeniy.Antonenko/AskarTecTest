﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AskarTecTest.Models.ClientModels
{
    public class ClientModel
    {
        [Display(Name = "ID:")]
        public int Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "BirthDate")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime BirthDate { get; set; }

        [Display(Name = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "SocialNumber")]
        public string SocialNumber { get; set; }
    }
}
