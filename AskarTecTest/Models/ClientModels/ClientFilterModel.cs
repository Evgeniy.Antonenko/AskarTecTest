﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AskarTecTest.Models.ClientModels
{
    public class ClientFilterModel
    {
        [Display(Name = "По Имени, ИНН, Номену телефона")]
        public string SearchKey { get; set; }

        public List<ClientModel> Clients { get; set; }

        public int? Page { get; set; }

        public ClientPageModel ClientPageModel { get; set; }

        public ClientModel Client { get; set; }

        public int Qty { get; set; }
    }
}
