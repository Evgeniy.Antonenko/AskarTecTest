﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AskarTecTest.Models.ClientModels
{
    public class ClientDetailsModel
    {
        [Display(Name = "ID:")]
        public int Id { get; set; }

        [Display(Name = "Фамилия, Имя:")]
        public string Name { get; set; }

        [Display(Name = "Дата рождения:")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        [Display(Name = "Номер телефона:")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Адрес клиента:")]
        public string Address { get; set; }

        [Display(Name = "ИНН:")]
        public string SocialNumber { get; set; }

        public string Message { get; set; }
    }
}
