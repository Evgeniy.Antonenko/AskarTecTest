﻿using OfficeOpenXml;
using System.IO;
using System.Linq;

namespace AskarTecTest.Excel
{
    public class ExcelReader
    {
        private string Path { get; }
        private string SheetName { get; }
        private FileInfo FileInfo { get; }
        private ExcelPackage ExcelPackage { get; }
        private ExcelWorksheet WorkSheet { get; }

        public ExcelReader(string path, string sheetName)
        {
            Path = path;
            SheetName = sheetName;
            FileInfo = new FileInfo(path);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage = new ExcelPackage(FileInfo);
            if (ExcelPackage.Workbook.Worksheets.FirstOrDefault(p => p.Name == sheetName) != null)
            {
                WorkSheet = ExcelPackage.Workbook.Worksheets.First(p => p.Name == sheetName);
            }
            else
            {
                WorkSheet = ExcelPackage.Workbook.Worksheets.Add(sheetName);
            }
        }

        public void WriteCell(string fileName, string content, int row, int column)
        {
            using (ExcelRange Rng = WorkSheet.Cells[row, column])
            {
                Rng.Value = content;

                WorkSheet.Protection.IsProtected = false;
                WorkSheet.Protection.AllowSelectLockedCells = false;
                ExcelPackage.SaveAs(new FileInfo($@"Result\output_{fileName}.xlsx"));
            }
        }
    }
}
