﻿using System;
using System.Threading.Tasks;
using AskarTecTest.DAL.Entities;
using AskarTecTest.Models.ClientModels;
using AskarTecTest.Services.Clients.Contracts;
using AskarTecTest.Validation;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AskarTecTest.Controllers
{
    public class ClientController : Controller
    {
        private readonly IClientService _clientService;
        private readonly UserManager<User> _userManager;

        public ClientController(IClientService clientService, UserManager<User> userManager)
        {
            if (clientService == null)
                throw new ArgumentNullException(nameof(clientService));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _clientService = clientService;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index(ClientFilterModel model, string search)
        {
            try
            {
                model.SearchKey = search;
                var clients = _clientService.GetClientFilterModel(model);
                return View(clients);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        public IActionResult AutoFillingClientsData()
        {
            _clientService.AutoFillingClients();
            return RedirectToAction("Index", "Client");
        }

        [HttpGet]
        public IActionResult CreateClient()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateClient(ClientCreateModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _clientService.CreateClient(model);
                    if (result.IsSuccess)
                    {
                        return RedirectToAction("Index", "Client");
                    }
                    else
                    {
                        UpdateModelState(result);

                        return View(nameof(CreateClient), model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        public IActionResult EditClient(int? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Client id cannot be NULL";
                return View("BadRequest");
            }

            var clientEditModel = _clientService.GetClientById(id.Value);

            return View(clientEditModel);
        }

        [HttpPost]
        public IActionResult EditClient(ClientEditModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _clientService.EditClient(model);
                    if (result.IsSuccess)
                    {
                        return RedirectToAction("Index", "Client");
                    }
                    else
                    {
                        UpdateModelState(result);

                        return View(nameof(EditClient), model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> DeleteClient(int? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Client id cannot be NULL";
                return View("BadRequest");
            }
            bool isDeleted = await _clientService.DeleteClientAsync(id.Value);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DetailsClient(int id)
        {
            var clientDetailsModel = _clientService.GetClientModelById(id);

            return View(clientDetailsModel);
        }

        [HttpPost]
        public IActionResult ExportClient(int? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Client id cannot be NULL";
                return View("BadRequest");
            }
            string message = _clientService.DownloadExcelFile(id.Value);
            return Ok(message);
        }

        private void UpdateModelState(ValidationResult result)
        {
            foreach (var message in result.ErrorMessages)
            {
                ModelState.AddModelError(string.Empty, message);
            }
        }
    }
}
