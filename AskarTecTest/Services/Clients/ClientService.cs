﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AskarTecTest.DAL;
using AskarTecTest.DAL.Entities;
using AskarTecTest.Excel;
using AskarTecTest.Models.ClientModels;
using AskarTecTest.Services.Clients.Contracts;
using AskarTecTest.Validation;
using AskarTecTest.Validation.Client;
using AutoMapper;

namespace AskarTecTest.Services.Clients
{
    public class ClientService : IClientService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly ClientCreateValidator _clientCreateValidator;
        private readonly ClientEditValidator _clientEditValidator;

        public ClientService(IUnitOfWorkFactory unitOfWorkFactory, ClientCreateValidator clientCreateValidator, ClientEditValidator clientEditValidator)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            if (clientCreateValidator == null)
                throw new ArgumentNullException(nameof(clientCreateValidator));
            if (clientEditValidator == null)
                throw new ArgumentNullException(nameof(clientEditValidator));

            _unitOfWorkFactory = unitOfWorkFactory;
            _clientCreateValidator = clientCreateValidator;
            _clientEditValidator = clientEditValidator;
        }

        public ClientService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public ClientFilterModel GetClientFilterModel(ClientFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Client> clients = unitOfWork.Clients.GetAll().ToList();
                var qtyClient = clients.Count();

                clients = clients
                    .BySearchKey(model.SearchKey);

                List<ClientModel> placeModels = Mapper.Map<List<ClientModel>>(clients);

                int pageSize = 5;
                int count = clients.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                ClientPageModel placePageModel = new ClientPageModel(count, page, pageSize);
                placeModels = placeModels.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                model.ClientPageModel = placePageModel;

                model.Clients = placeModels;

                model.Qty = qtyClient;

                return model;
            }
        }

        public void AutoFillingClients()
        {
            List<Client> clients = GetClientListForAutoFill();
            using (var unifOfWork = _unitOfWorkFactory.Create())
            {
                foreach (var client in clients)
                {
                    unifOfWork.Clients.Create(client);
                }
            }
        }

        public ValidationResult CreateClient(ClientCreateModel model)
        {
            model.Name = Regex.Replace(model.Name, @"\s+", " ").Trim();
            model.Address = Regex.Replace(model.Address, @"\s+", " ").Trim();
            var validationResult = _clientCreateValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    var client = Mapper.Map<Client>(model);

                    unitOfWork.Clients.Create(client);
                }
            }
            return validationResult;
        }

        public ClientEditModel GetClientById(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var client = unitOfWork.Clients.GetById(id);
                var model = Mapper.Map<ClientEditModel>(client);
                return model;
            }
        }

        public ValidationResult EditClient(ClientEditModel model)
        {
            model.Name = Regex.Replace(model.Name, @"\s+", " ").Trim();
            model.Address = Regex.Replace(model.Address, @"\s+", " ").Trim();
            var validationResult = _clientEditValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    var client = Mapper.Map<Client>(model);

                    unitOfWork.Clients.Update(client);
                }
            }
            return validationResult;
        }

        public async Task<bool> DeleteClientAsync(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var client = await unitOfWork.Clients.GetByIdAsync(id);
                try
                {
                    unitOfWork.Clients.Remove(client);
                    await unitOfWork.CompleteAsync();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public ClientDetailsModel GetClientModelById(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var client = unitOfWork.Clients.GetById(id);
                var model = Mapper.Map<ClientDetailsModel>(client);
                return model;
            }
        }

        public string DownloadExcelFile(int id)
        {
            ExcelReader excel = new ExcelReader(@"Template\example.xlsx", "Лист1");
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                Client client = unitOfWork.Clients.GetById(id);
                string fileName = $"{client.Name}_{client.SocialNumber}";

                excel.WriteCell(fileName, DateTime.Now.ToString(), 1, 2);
                excel.WriteCell(fileName, client.Id.ToString(), 3, 2);
                excel.WriteCell(fileName, client.Name, 4, 2);
                excel.WriteCell(fileName, client.BirthDate.ToShortDateString(), 5, 2);
                excel.WriteCell(fileName, client.PhoneNumber, 6, 2);
                excel.WriteCell(fileName, client.Address, 7, 2);
                excel.WriteCell(fileName, client.SocialNumber, 8, 2);

                excel.WriteCell(fileName, client.Id.ToString(), 4, 4);
                excel.WriteCell(fileName, client.Name, 4, 5);
                excel.WriteCell(fileName, client.BirthDate.ToShortDateString(), 4, 6);
                excel.WriteCell(fileName, client.PhoneNumber, 4, 7);
                excel.WriteCell(fileName, client.Address, 4, 8);
                excel.WriteCell(fileName, client.SocialNumber, 4, 9);

                return $"Client data: {client.Name}, saved to file: output_{fileName}";
            }
        }

        public List<Client> GetClientListForAutoFill()
        {
            List<Client> clients = new List<Client>()
            {
                new Client()
                {
                    Name = "Тестовый Клиент Первый",
                    BirthDate = new DateTime(1991, 03, 08),
                    PhoneNumber = "+996312123123",
                    Address = "г. Баткен",
                    SocialNumber = "10803199101234"
                },
                new Client()
                {
                    Name = "Тестовый Клиент Второй",
                    BirthDate = new DateTime(1996, 04, 20),
                    PhoneNumber = "+996312456456",
                    Address = "г. Бишкек",
                    SocialNumber = "22004199601234"
                },
                new Client()
                {
                    Name = "Тестовый Клиент Третий",
                    BirthDate = new DateTime(1995, 08, 04),
                    PhoneNumber = "+996312789789",
                    Address = "г. Нарын",
                    SocialNumber = "10408199511234"
                },
                new Client()
                {
                    Name = "Тестовый Клиент Четвертый",
                    BirthDate = new DateTime(1989, 02, 25),
                    PhoneNumber = "+996312012012",
                    Address = "с. Комсомольское",
                    SocialNumber = "12502198934567"
                }
            };
            return clients;
        }
    }
}
