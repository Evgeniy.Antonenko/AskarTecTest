﻿using System.Collections.Generic;
using System.Linq;
using AskarTecTest.DAL.Entities;

namespace AskarTecTest.Services.Clients
{
    public static class ClientServiceExtensions
    {
        public static IEnumerable<Client> BySearchKey(this IEnumerable<Client> clients, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                clients = clients.Where(c => c.Name.Contains(searchKey) || c.SocialNumber.Contains(searchKey) || c.PhoneNumber.Contains(searchKey));

            return clients;
        }
    }
}
