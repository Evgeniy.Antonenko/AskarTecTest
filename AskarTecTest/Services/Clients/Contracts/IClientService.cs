﻿using System.Threading.Tasks;
using AskarTecTest.Models.ClientModels;
using AskarTecTest.Validation;

namespace AskarTecTest.Services.Clients.Contracts
{
    public interface IClientService
    {
        string DownloadExcelFile(int id);
        ClientDetailsModel GetClientModelById(int id);
        Task<bool> DeleteClientAsync(int id);
        ValidationResult EditClient(ClientEditModel model);
        ClientEditModel GetClientById(int id);
        ValidationResult CreateClient(ClientCreateModel model);
        void AutoFillingClients();
        ClientFilterModel GetClientFilterModel(ClientFilterModel model);
    }
}
