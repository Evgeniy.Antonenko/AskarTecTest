﻿using AskarTecTest.DAL.Entities;
using AskarTecTest.Models.ClientModels;
using AutoMapper;

namespace AskarTecTest
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateModelMap<Client, ClientModel>();
            CreateModelMap<Client, ClientCreateModel>();
            CreateModelMap<Client, ClientEditModel>();
            CreateModelMap<Client, ClientDetailsModel>();
        }

        private void CreateModelMap<From, To>()
        {
            CreateMap<From, To>();
            CreateMap<To, From>();
        }
    }
}
